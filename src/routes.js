import {app} from './init';

// Handle routes
app.get('/', (req, res) => {
    res.send('Hello world node.js es6 app.');
});