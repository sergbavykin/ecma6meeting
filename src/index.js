import express from 'express';
import {app} from './init';
import {getTriangleSquare} from './geomethry';


// Here routes defined
import './routes';
app.listen(3000, () => {
    (function () {
        'use strict';

        class Shape {

            constructor(title, area) {
                this.title = title;
                this.area = area;
            }

            // The way to determine abstract class
            /*constructor() {
                if(new.target == Shape) {
                    throw new TypeError("Cannot construct Shape instances directly");
                }
            }*/

            // The way to determine abstract method in class
            /*constructor() {
                if(this.getArea === undefined) {
                    throw new TypeError("getArea is abstract and must be overridden");
                }
            }*/

            getTitle() {
                return this.title;
            }

            getArea() {
                return this.area;
            }

        }

        class Triangle extends Shape {

            constructor(a, b, c) {
                /*super('This is a triangle', () => {
                 // Geron expression should be here
                 });*/

                super('This is a triangle', getTriangleSquare(/* new language feature */...[a, b, c]));

                this.a = a;
                this.b = b;
                this.c = c;
            }

        }

        // Define a triangle with 90* angle
        var directTriangle = new Triangle(3, 4, 5);

        console.log(directTriangle.getArea());

    }())
});